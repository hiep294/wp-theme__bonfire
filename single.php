<?php
get_header();
// If there are any posts
if (have_posts()) :

    // Load posts loop
    while (have_posts()) : the_post();
?>
        <main>
            <section class="blogs blogs-detail">
                <div class="blogs__header">
                    <h2 class="title"><? the_title() ?></h2>
                </div>
                <? the_post_thumbnail('blog-thumbnail-portrait', array(
                    'class' => 'thumb',
                    'alt' => 'blog-thumbnail'
                )); ?>
                <div class="blog-item__date blog-item__padding container">
                    <? the_time('F jS, Y') ?>
                </div>
                <div class="container content">
                    <? the_content() ?>
                </div>
                <div class="blogs-btns">
                    <a href="<? echo get_post_type_archive_link('post'); ?>" class="blogs-btn">
                        <span>Go to blog</span>
                    </a>
                </div>
            </section>
        </main>
    <?php
    endwhile;
else :
    ?>
    <p>Nothing to display.</p>
<?php endif;
get_footer(); ?>