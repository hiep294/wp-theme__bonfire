<?
// Customizer additions.
require get_template_directory() . '/classes/class-bonfire-customize.php';
new Bonfire_Customize();

// Customizer nav menu.
require get_template_directory() . '/classes/class-bonfire-nav-icon-walker.php';

// Widgets
require get_template_directory() . '/classes/class-slider-widget.php';
require get_template_directory() . '/classes/class-intro-service-widget.php';
require get_template_directory() . '/classes/class-feature-item1-widget.php';
require get_template_directory() . '/classes/class-feature-item2-widget.php';
require get_template_directory() . '/classes/class-feature-item3-widget.php';
require get_template_directory() . '/classes/class-feature-item4-widget.php';
require get_template_directory() . '/classes/class-feature-item6-widget.php';
require get_template_directory() . '/classes/class-subscribe-widget.php';

function loadStylesScripts()
{
    wp_enqueue_style('custom_css_4', get_theme_file_uri('/assets/style1.css'));
    wp_enqueue_style('wp-custom-style-2', get_theme_file_uri('/assets/wp-custom-style2.css'));
    wp_enqueue_style('woocommerce', get_theme_file_uri('/assets/woocommerce.css'), array(), filemtime(get_template_directory() . '/assets/woocommerce.css'), 'all');
    wp_enqueue_style('contact-form-7-custom', get_theme_file_uri('/assets/contact-form-7.css'), array(), filemtime(get_template_directory() . '/assets/contact-form-7.css'), 'all');

    wp_enqueue_script('custom-script-1', get_theme_file_uri('/assets/js/modernizr-custom.js'), NULL, filemtime(get_template_directory() . '/assets/js/modernizr-custom.js'), true);
    wp_enqueue_script('custom-script-2', get_theme_file_uri('/assets/js/carousel-slider.js'), NULL, filemtime(get_template_directory() . '/assets/js/carousel-slider.js'), true);
}

function bonfire_setup()
{
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    $imageCropable = true;
    add_image_size('blog-thumbnail-portrait', 370, 220, $imageCropable);
    add_image_size('product-thumbnail', 270, 320, $imageCropable);

    add_theme_support('customize-selective-refresh-widgets');

    register_nav_menus(
        array(
            // 'primary' => esc_html__('Primary menu', 'bonfire'),
            'footer'  => __('Secondary Menu', 'bonfire'),
        )
    );

    register_nav_menus(
        array(
            // 'primary' => esc_html__('Primary menu', 'bonfire'),
            'socials'  => __('Socials Menu', 'bonfire'),
        )
    );
}

function widgetsInit()
{
    /**
     * widget AREAS
     */
    // Slider
    register_sidebar(array(
        'name' => 'Slider',
        'id' => 'slider'
    ));

    // intro services
    register_sidebar(array(
        'name' => 'Intro Services',
        'id' => 'intro-services'
    ));

    // Guideline
    register_sidebar(array(
        'name' => 'Guideline',
        'id' => 'guideline'
    ));

    // feature items
    register_sidebar(array(
        'name' => 'Feature Item 1',
        'id' => 'feature-item-1'
    ));
    register_sidebar(array(
        'name' => 'Feature Item 2',
        'id' => 'feature-item-2'
    ));
    register_sidebar(array(
        'name' => 'Feature Item 3',
        'id' => 'feature-item-3'
    ));
    register_sidebar(array(
        'name' => 'Feature Item 4',
        'id' => 'feature-item-4'
    ));
    register_sidebar(array(
        'name' => 'Feature Item 5',
        'id' => 'feature-item-5'
    ));
    register_sidebar(array(
        'name' => 'Feature Item 6',
        'id' => 'feature-item-6'
    ));

    // brands
    register_sidebar(array(
        'name' => 'Brands',
        'id' => 'brands',
        'before_widget'  => '<div id="%1$s" class="col-4 col-md-2">',
        'after_widget'   => "</div>\n",
    ));

    // footer
    register_sidebar(array(
        'name' => 'Footer Subscribe',
        'id' => 'footer-subscribe'
    ));
    register_sidebar(array(
        'name' => 'Footer Contact',
        'id' => 'footer-contact'
    ));
    register_sidebar(array(
        'name' => 'Footer Copyright',
        'id' => 'footer-copyright'
    ));


    /**
     * widgets
     */
    register_widget('Bonfire_Slider_Widget');
    register_widget('Bonfire_Intro_Service_Widget');
    register_widget('Bonfire_Feature_Item1_Widget');
    register_widget('Bonfire_Feature_Item2_Widget');
    register_widget('Bonfire_Feature_Item3_Widget');
    register_widget('Bonfire_Feature_Item4_Widget');
    register_widget('Bonfire_Feature_Item6_Widget');
    register_widget('Bonfire_Subscribe_Widget');
}

/**
 * Show cart contents / total Ajax
 */

function woocommerce_header_add_to_cart_fragment($fragments)
{
    ob_start();
    $countCartItems = WC()->cart->get_cart_contents_count();
    if ($countCartItems) {
?>
        <span class="badge cart-customlocation">
            <?php echo $countCartItems; ?>
        </span>
    <?
    } else {
    ?>
        <span class="cart-customlocation">
        </span>
<?
    }
    $fragments['span.cart-customlocation'] = ob_get_clean();
    return $fragments;
}
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');


add_action('wp_enqueue_scripts', 'loadStylesScripts');
add_action('after_setup_theme', 'bonfire_setup');
add_action('widgets_init', 'widgetsInit');
