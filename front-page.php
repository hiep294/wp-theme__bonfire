<?
get_header();
$homepagePosts = new WP_Query(array(
    'posts_per_page' => 3,
));
$homepageProducts = new WP_Query(array(
    'posts_per_page' => 8,
    'post_type' => 'product'
));
?>

<main>

    <? if (is_active_sidebar('slider')) : ?>
        <? dynamic_sidebar('slider'); ?>
    <? endif ?>

    <? if (is_active_sidebar('intro-services') || is_active_sidebar('guideline')) : ?>
        <section class="intro">
            <? if (is_active_sidebar('intro-services')) : ?>
                <div class="container">
                    <ul class="intro__services">
                        <? dynamic_sidebar('intro-services') ?>
                    </ul>
                </div>
                <div class="intro__divider"></div>
            <? endif ?>
            <? if (is_active_sidebar('guideline')) : ?>
                <div class="container">
                    <h2 class="intro__guide-line">
                        <? dynamic_sidebar('guideline') ?>
                    </h2>
                </div>
            <? endif ?>
        </section>
    <? endif ?>


    <? if (is_active_sidebar('feature-item-1') || is_active_sidebar('feature-item-2') || is_active_sidebar('feature-item-3') || is_active_sidebar('feature-item-4') || is_active_sidebar('feature-item-5') || is_active_sidebar('feature-item-6')) : ?>
        <section class="feature-items">
            <div class="container-big">
                <div class="row gutter-xxl-50">
                    <? if (is_active_sidebar('feature-item-1')) : ?>
                        <div class="col-12 col-sm-6 col-md-3">
                            <? dynamic_sidebar('feature-item-1'); ?>
                        </div>
                    <? endif ?>
                    <? if (is_active_sidebar('feature-item-2')) : ?>
                        <div class="col-0 col-sm-0 col-md-6">
                            <? dynamic_sidebar('feature-item-2'); ?>
                        </div>
                    <? endif ?>
                    <? if (is_active_sidebar('feature-item-3')) : ?>
                        <div class="col-12 col-sm-6 col-md-3">
                            <? dynamic_sidebar('feature-item-3'); ?>
                        </div>
                    <? endif ?>
                    <? if (is_active_sidebar('feature-item-4')) : ?>
                        <div class="col-12 col-sm-6 col-md-3">
                            <? dynamic_sidebar('feature-item-4'); ?>
                        </div>
                    <? endif ?>

                    <? if (is_active_sidebar('feature-item-5')) : ?>
                        <div class="col-12 col-sm-6 col-md-3">
                            <? dynamic_sidebar('feature-item-5'); ?>
                        </div>
                    <? endif ?>

                    <? if (is_active_sidebar('feature-item-2')) : ?>
                        <div class="col-12 col-sm-12 col-md-0">
                            <? dynamic_sidebar('feature-item-2'); ?>
                        </div>
                    <? endif ?>

                    <? if (is_active_sidebar('feature-item-6')) : ?>
                        <div class="col-12 col-sm-12 col-md-6">
                            <? dynamic_sidebar('feature-item-6'); ?>
                        </div>
                    <? endif ?>
                </div>
            </div>
        </section>
    <? endif ?>

    <? if ($homepageProducts->post_count) : ?>
        <section class="feature-products">
            <div class="container">
                <div class="feature-products__heading">
                    <div class="feature-products__heading__container">
                        <h2 class="title">Featured items</h2>
                        <a href="<? echo get_site_url(null, '/shop') ?>" class="view-more">View more</a>
                    </div>
                </div>
                <div class="feature-products__body">
                    <? echo do_shortcode('[products limit="8"]'); ?>
                </div>
            </div>
        </section>
    <? endif ?>

    <? if ($homepagePosts->post_count) : ?>
        <section class="blogs">
            <div class="blogs__header">
                <h2 class="title">Blog update</h2>
            </div>
            <div class="container">
                <div class="row">
                    <?
                    while ($homepagePosts->have_posts()) {
                        $homepagePosts->the_post();
                        get_template_part('template-parts/post');
                    } ?>
                </div>
            </div>
            <div class="blogs-btns">
                <a href="<? echo get_post_type_archive_link('post') ?>" class="blogs-btn">
                    <span>Go to blog</span>
                </a>
            </div>
        </section>
    <? endif ?>


    <? if (is_active_sidebar('brands')) : ?>
        <section class="brands">
            <div class="container">
                <div class="row brands__list">
                    <? dynamic_sidebar('brands'); ?>
                </div>
            </div>
        </section>
    <? endif ?>
</main>
<? get_footer();
