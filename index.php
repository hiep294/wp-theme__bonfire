<?php

/**
 * This is used for only blogs page
 */
global $wp_query;
get_header();
?>
<section class="blogs">
    <div class="blogs__header">
        <h2 class="title">Blog update</h2>
    </div>
    <div class="container">
        <div class="row">

            <?
            if ($wp_query->found_posts) {
                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/post');
                }
            } else {
                echo 'No data has found';
            }
            ?>
        </div>
    </div>
    <div class="blogs-btns">
        <? echo paginate_links(); ?>
    </div>
</section>

<?php get_footer();

?>