<?
$footerLogoSrc = get_theme_mod('footer_logo');
?>
<footer class="footer">
    <div class="container">
        <div class="footer__group-1">
            <div class="logo-container">
                <? if ($footerLogoSrc) { ?>
                    <img src="<? echo $footerLogoSrc; ?>" alt="logo_footer" class="logo" />
                <? } ?>
            </div>


            <nav aria-label="<?php esc_attr_e('Secondary menu', 'bonfire'); ?>" class="footer-navigation">
                <?php if (has_nav_menu('footer')) : ?>
                    <ul class="links footer-navigation-wrapper">
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'footer',
                                'items_wrap'     => '%3$s',
                                'container'      => false,
                                'depth'          => 1,
                                'fallback_cb'    => false,
                            )
                        );
                        ?>
                    </ul>
                <?php endif; ?>
            </nav>
            <? if (is_active_sidebar('footer-subscribe')) : ?>
                <p>
                    <? dynamic_sidebar('footer-subscribe') ?>
                </p>
            <? endif ?>
        </div>
        <div class="line"></div>
        <div class="footer__group-2">
            <div class="address">
                <? if (is_active_sidebar('footer-contact')) : ?>
                    <p>
                        <? dynamic_sidebar('footer-contact') ?>
                    </p>
                <? endif ?>
                <? if (is_active_sidebar('footer-copyright')) : ?>
                    <span class="copy-right hidden-max-xl">
                        <? dynamic_sidebar('footer-copyright') ?>
                    </span>
                <? endif ?>
            </div>
            <ul class="socials">
                <?php if (has_nav_menu('socials')) : ?>
                    <?php
                    $bonfire_nav_icon_walker = new Bonfire_Nav_Icon_Walker;
                    wp_nav_menu(
                        array(
                            'theme_location' => 'socials',
                            'items_wrap'     => '%3$s',
                            'container'      => false,
                            'depth'          => 1,
                            'fallback_cb'    => false,
                            'walker' => $bonfire_nav_icon_walker
                        )
                    );
                    ?>
                <?php endif; ?>
            </ul>

        </div>
        <div class="footer__group-3 hidden-xl">
            <? if (is_active_sidebar('footer-copyright')) : ?>
                <div class="copy-right"><? dynamic_sidebar('footer-copyright'); ?></div>
            <? endif ?>
        </div>
    </div>
</footer>
<?php  ?>

<? wp_footer(); ?>


</body>

</html>