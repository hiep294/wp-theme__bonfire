<?
$headerLogoSrc = get_theme_mod('header_logo');
?>

<!DOCTYPE html>
<html <? language_attributes(); ?>>

<head>
    <meta charset="<? bloginfo('charset'); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="<? echo get_theme_file_uri('/assets/images/favicon.ico') ?>" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="header">
        <div class="container-big">
            <div class="header__menu">
                <h1>
                    <?
                    if ($headerLogoSrc) {
                    ?>
                        <a href="<? echo get_site_url(); ?>">
                            <img src="<? echo $headerLogoSrc; ?>" alt="logo" class="logo" />
                        </a>
                    <?
                    }
                    ?>
                </h1>
                <div class="menu__right">
                    <div class="menu__right__item">
                        <a href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('View your shopping cart'); ?>">
                            <img src="<? echo get_theme_file_uri('/assets/images/icons/cart.png') ?>" alt="cart" />
                            <?
                            $countCartItems = WC()->cart->get_cart_contents_count();
                            ?>
                            <span class="<? if ($countCartItems) echo 'badge'; ?> cart-customlocation">
                                <?php if ($countCartItems) echo $countCartItems; ?>
                            </span>
                        </a>
                    </div>

                    <div class="menu__right__item">
                        <a href="#">
                            <img src="<? echo get_theme_file_uri('/assets/images/icons/menu.png') ?>" alt="cart" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <? if (!is_front_page()) : ?>
        <div class="header-bg"></div>
    <? endif; ?>