<?


class Bonfire_Subscribe_Widget extends WP_Widget
{
    public $defaultData = array(
        'buttonText' => '',
    );
    function __construct()
    {
        parent::__construct(
            'Bonfire_Subscribe_Widget', // id
            'Subscribe Widget', // name of widget
            array(
                'description' => 'Subscribe Widget'
            )
        );
    }

    // setting fields
    function form($instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance
        $buttonText = esc_attr($instance['buttonText']);

        echo '<div>';

        echo ('Button Text: <input type="text" class="widefat" name="' . $this->get_field_name('buttonText') . '" value="' . $buttonText . '">');

        echo '</div>';
    }

    // save data
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['buttonText'] = $new_instance['buttonText'];
        return $instance;
    }

    // show widget to frontend
    function widget($args, $instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        extract($instance);

?>
        <a href="#" class="subscribe">
            <span><? echo $buttonText; ?></span>
            <img src="<? echo  get_theme_file_uri('/assets/images/icons/email.png'); ?>" alt="" />
        </a>
<?

        echo $after_widget;
    }
}
