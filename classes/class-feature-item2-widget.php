<?


class Bonfire_Feature_Item2_Widget extends WP_Widget
{
    public $defaultData = array(
        'topText' => '',
        'bigText' => '',
        'title' => '',
        'buttonText' => '',
        'backgroundUrl' => '',
        'link' => ''
    );

    function __construct()
    {
        parent::__construct(
            'Bonfire_Feature_Item2_Widget', // id
            'Feature Item2 Widget', // name of widget
            array(
                'description' => 'Feature Item1 Widget'
            )
        );
    }

    // setting fields
    function form($instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        $topText = esc_attr($instance['topText']);
        $bigText = esc_attr($instance['bigText']);
        $title = esc_attr($instance['title']);
        $buttonText = esc_attr($instance['buttonText']);
        $backgroundUrl = esc_attr($instance['backgroundUrl']);
        $link = esc_attr($instance['link']);

        echo '<div>';

        echo ('Top Text: <input type="text" class="widefat" name="' . $this->get_field_name('topText') . '" value="' . $topText . '">');
        echo ('Big Text: <input type="text" class="widefat" name="' . $this->get_field_name('bigText') . '" value="' . $bigText . '">');
        echo ('Title: <input type="text" class="widefat" name="' . $this->get_field_name('title') . '" value="' . $title . '">');
        echo ('Button Text: <input type="text" class="widefat" name="' . $this->get_field_name('buttonText') . '" value="' . $buttonText . '">');
        echo ('Background Url: <input type="text" class="widefat" name="' . $this->get_field_name('backgroundUrl') . '" value="' . $backgroundUrl . '">');
        echo ('Link: <input type="text" class="widefat" name="' . $this->get_field_name('link') . '" value="' . $link . '">');

        echo '</div>';
    }

    // save data
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['topText'] = $new_instance['topText'];
        $instance['bigText'] = $new_instance['bigText'];
        $instance['title'] = $new_instance['title'];
        $instance['buttonText'] = $new_instance['buttonText'];
        $instance['backgroundUrl'] = $new_instance['backgroundUrl'];
        $instance['link'] = $new_instance['link'];
        return $instance;
    }

    // show widget to frontend
    function widget($args, $instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        extract($instance);

        if (!(strpos($backgroundUrl, 'http') >= 0) && $backgroundUrl != '') {
            $backgroundUrl = get_site_url() . $backgroundUrl;
        }

        if (!$backgroundUrl) {
            $backgroundUrl = get_theme_file_uri('/assets/images/feature-item-2.jpg');
        }
?>
        <div class="feature-item-2">
            <img src="<? echo $backgroundUrl; ?>" alt="feature-item-2" />
            <div class="text-wrapper">
                <img src="<? echo get_theme_file_uri('/assets/images/feature-item-2__bg-text.png') ?>" alt="" />
                <div class="text-container">
                    <div class="text__sale-up-to"><? echo $topText; ?></div>
                    <div class="group-1">
                        <div class="text__percent">
                            <a href="<? echo $link; ?>">
                                <? echo $bigText; ?>
                            </a>
                        </div>
                        <div class="text__title">
                            <a href="<? echo $link; ?>">
                                <? echo $title; ?>
                            </a>
                        </div>
                    </div>
                    <a href="<? echo $link; ?>" class="text__shop-now"><? echo $buttonText; ?></a>
                </div>
            </div>
        </div>
<?

        echo $after_widget;
    }
}
