<?


class Bonfire_Intro_Service_Widget extends WP_Widget
{
    public $defaultData = array(
        'title' => '',
        'subTitle' => '',
    );

    function __construct()
    {
        parent::__construct(
            'Bonfire_Intro_Service_Widget', // id
            'Intro Service Widget', // name of widget
            array(
                'description' => 'widget for Intro Service'
            )
        );
    }

    // setting fields
    function form($instance)
    {

        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance
        $title = esc_attr($instance['title']);
        $subTitle = esc_attr($instance['subTitle']);

        echo '<div>';
        echo ('Title: <input type="text" class="widefat" name="' . $this->get_field_name('title') . '" value="' . $title . '">');

        echo ('Sub Title: <input type="text" class="widefat" name="' . $this->get_field_name('subTitle') . '" value="' . $subTitle . '">');

        echo '</div>';
    }

    // save data
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = $new_instance['title'];
        $instance['subTitle'] = $new_instance['subTitle'];
        return $instance;
    }

    // show widget to frontend
    function widget($args, $instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        extract($instance);

?>
        <li class="intro__service">
            <div class="title"><? echo $title; ?></div>
            <div class="description"><? echo $subTitle; ?></div>
        </li>
<?

        echo $after_widget;
    }
}
