<?


class Bonfire_Slider_Widget extends WP_Widget
{
    public $defaultData = array(
        'date' => "",
        'title' => '',
        'backgroundUrl' => '',
        'buttonText' => '',
    );

    function __construct()
    {
        parent::__construct(
            'slider_widget', // id
            'Slider Widget', // name of widget
            array(
                'description' => 'widget for slider'
            )
        );
    }

    // setting fields
    function form($instance)
    {

        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance
        $title = esc_attr($instance['title']);
        $date = esc_attr($instance['date']);
        $backgroundUrl = esc_attr($instance['backgroundUrl']);
        $buttonText = esc_attr($instance['buttonText']);

        echo '<div>';
        echo ('Date: <input type="text" class="widefat" name="' . $this->get_field_name('date') . '" value="' . $date . '">');

        echo ('Title: <input type="text" class="widefat" name="' . $this->get_field_name('title') . '" value="' . $title . '">');

        echo ('Background Url: <input type="text" class="widefat" name="' . $this->get_field_name('backgroundUrl') . '" value="' . $backgroundUrl . '">');

        echo ('Button Text: <input type="text" class="widefat" name="' . $this->get_field_name('buttonText') . '" value="' . $buttonText . '">');
        echo '</div>';
    }

    // save data
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['date'] = $new_instance['date'];
        $instance['title'] = $new_instance['title'];
        $instance['backgroundUrl'] = $new_instance['backgroundUrl'];
        $instance['buttonText'] = $new_instance['buttonText'];
        return $instance;
    }

    // show widget to frontend
    function widget($args, $instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        extract($instance);

        if (!(strpos($backgroundUrl, 'http') >= 0) && $backgroundUrl != '') {
            $backgroundUrl = get_site_url() . $backgroundUrl;
        }
?>
        <section class="slider" <? if ($backgroundUrl) {
                                    echo 'style="background-image: url(' . $backgroundUrl . ')"';
                                } ?>>
            <div class="container-big">
                <div class="slider-content">
                    <div class="slider-content__group-1">
                        <div class="slider-heading">
                            <div class="slider-date"><? echo $date ?></div>
                            <h2 class="slider-slogan"><? echo $title; ?></h2>
                            <a href="#" class="button slider-btn-layout"><? echo $buttonText; ?></a>
                        </div>
                        <ul class="slider-socials">
                            <?php if (has_nav_menu('socials')) : ?>
                                <?php
                                wp_nav_menu(
                                    array(
                                        'theme_location' => 'socials',
                                        'items_wrap'     => '%3$s',
                                        'container'      => false,
                                        'depth'          => 1,
                                        'fallback_cb'    => false,
                                    )
                                );
                                ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
<?

        echo $after_widget;
    }
}
