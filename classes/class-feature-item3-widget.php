<?


class Bonfire_Feature_Item3_Widget extends WP_Widget
{
    public $defaultData = array(
        'title' => '',
        'subTitle' => '',
        'backgroundUrl' => '',
        'link' => ''
    );

    function __construct()
    {
        parent::__construct(
            'Bonfire_Feature_Item3_Widget', // id
            'Feature Item3 Widget', // name of widget
            array(
                'description' => 'Feature Item3 Widget'
            )
        );
    }

    // setting fields
    function form($instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        $title = esc_attr($instance['title']);
        $subTitle = esc_attr($instance['subTitle']);
        $backgroundUrl = esc_attr($instance['backgroundUrl']);
        $link = esc_attr($instance['link']);

        echo '<div>';

        echo ('Title: <input type="text" class="widefat" name="' . $this->get_field_name('title') . '" value="' . $title . '">');

        echo ('Background Url: <input type="text" class="widefat" name="' . $this->get_field_name('backgroundUrl') . '" value="' . $backgroundUrl . '">');

        echo ('Sub Title: <input type="text" class="widefat" name="' . $this->get_field_name('subTitle') . '" value="' . $subTitle . '">');

        echo ('Link: <input type="text" class="widefat" name="' . $this->get_field_name('link') . '" value="' . $link . '">');
        echo '</div>';
    }

    // save data
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = $new_instance['title'];
        $instance['subTitle'] = $new_instance['subTitle'];
        $instance['backgroundUrl'] = $new_instance['backgroundUrl'];
        $instance['link'] = $new_instance['link'];
        return $instance;
    }

    // show widget to frontend
    function widget($args, $instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        extract($instance);

        if (!(strpos($backgroundUrl, 'http') >= 0) && $backgroundUrl != '') {
            $backgroundUrl = get_site_url() . $backgroundUrl;
        }

        if (!$backgroundUrl) {
            $backgroundUrl = get_theme_file_uri('/assets/images/feature-item-3.jpg');
        }
?>
        <div class="feature-item-3">
            <img src="<? echo $backgroundUrl; ?>" alt="feature-item-3" />
            <div class="text-container">
                <div class="new-collection"><? echo $subTitle; ?></div>
                <div class="left-ring">
                    <a href="<? echo $link; ?>">
                        <? echo $title; ?>
                    </a>
                </div>
            </div>
        </div>
<?

        echo $after_widget;
    }
}
