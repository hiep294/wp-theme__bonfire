<?


class Bonfire_Feature_Item4_Widget extends WP_Widget
{
    public $defaultData = array(
        'backgroundUrl' => '',
        'link' => ''
    );

    function __construct()
    {
        parent::__construct(
            'Bonfire_Feature_Item4_Widget', // id
            'Feature Item4 Widget', // name of widget
            array(
                'description' => 'Feature Item4 Widget'
            )
        );
    }

    // setting fields
    function form($instance)
    {

        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        $backgroundUrl = esc_attr($instance['backgroundUrl']);
        $link = esc_attr($instance['link']);

        echo '<div>';

        echo ('Background Url: <input type="text" class="widefat" name="' . $this->get_field_name('backgroundUrl') . '" value="' . $backgroundUrl . '">');

        echo ('Link: <input type="text" class="widefat" name="' . $this->get_field_name('link') . '" value="' . $link . '">');
        echo '</div>';
    }

    // save data
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['backgroundUrl'] = $new_instance['backgroundUrl'];
        $instance['link'] = $new_instance['link'];
        return $instance;
    }

    // show widget to frontend
    function widget($args, $instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        extract($instance);

        if (!(strpos($backgroundUrl, 'http') >= 0) && $backgroundUrl != '') {
            $backgroundUrl = get_site_url() . $backgroundUrl;
        }

        if (!$backgroundUrl) {
            $backgroundUrl = get_theme_file_uri('/assets/images/feature-item-4.jpg');
        }
?>
        <a href="<? echo $link; ?>">
            <img src="<? echo $backgroundUrl; ?>" alt="feature-item-4" />
        </a>
<?

        echo $after_widget;
    }
}
