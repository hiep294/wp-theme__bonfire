<?


class Bonfire_Feature_Item1_Widget extends WP_Widget
{
    public $defaultData = array(
        'title' => '',
        'price' => '',
        'backgroundUrl' => '',
        'link' => ''
    );
    function __construct()
    {
        parent::__construct(
            'Bonfire_Feature_Item1_Widget', // id
            'Feature Item1 Widget', // name of widget
            array(
                'description' => 'Feature Item1 Widget'
            )
        );
    }

    // setting fields
    function form($instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        $title = esc_attr($instance['title']);
        $price = esc_attr($instance['price']);
        $backgroundUrl = esc_attr($instance['backgroundUrl']);
        $link = esc_attr($instance['link']);

        echo '<div>';

        echo ('Title: <input type="text" class="widefat" name="' . $this->get_field_name('title') . '" value="' . $title . '">');

        echo ('Background Url: <input type="text" class="widefat" name="' . $this->get_field_name('backgroundUrl') . '" value="' . $backgroundUrl . '">');

        echo ('Price: <input type="text" class="widefat" name="' . $this->get_field_name('price') . '" value="' . $price . '">');

        echo ('Link: <input type="text" class="widefat" name="' . $this->get_field_name('link') . '" value="' . $link . '">');
        echo '</div>';
    }

    // save data
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = $new_instance['title'];
        $instance['price'] = $new_instance['price'];
        $instance['backgroundUrl'] = $new_instance['backgroundUrl'];
        $instance['link'] = $new_instance['link'];
        return $instance;
    }

    // show widget to frontend
    function widget($args, $instance)
    {
        $instance = wp_parse_args($instance, $this->defaultData); // apply $defaultData to $instance

        extract($instance);

        if (!(strpos($backgroundUrl, 'http') >= 0) && $backgroundUrl != '') {
            $backgroundUrl = get_site_url() . $backgroundUrl;
        }

        if (!$backgroundUrl) {
            $backgroundUrl = get_theme_file_uri('/assets/images/feature-item-1.jpg');
        }
?>
        <div class="feature-item-1">
            <img src="<? echo $backgroundUrl; ?>" alt="feature-item-1" />
            <div class="text-container">
                <div class="name">
                    <a href="<? echo $link; ?>">
                        <? echo $title; ?>
                    </a>
                </div>
                <div class="price">
                    <a href="<? echo $link; ?>">
                        <? echo $price; ?>
                    </a>
                </div>
            </div>
        </div>
<?

        echo $after_widget;
    }
}
