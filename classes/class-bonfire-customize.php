<?

if (!class_exists('Bonfire_Customize')) {
    class Bonfire_Customize
    {
        /**
         * Constructor. Instantiate the object.
         *
         * @access public
         *
         * @since Twenty Twenty-One 1.0
         */
        public function __construct()
        {
            // add_action('customize_register', array($this, 'registerHeader'));
            // add_action('customize_register', array($this, 'registerFooter'));
            add_action('customize_register', array($this, 'registerLogo'));
        }

        public function registerLogo($wp_customize)
        {
            //Header
            $wp_customize->add_section("logo", array(
                'title'         => __("Logo", "devvn"),
                'priority'         => 130,
                'description'     => __('Custom logo for the footer and the header'),
            ));

            //Header logo
            $wp_customize->add_setting("header_logo", array(
                'default' => '',
            ));
            $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize, 'header_logo', array(
                'label'          => __('Header Logo', 'devvn'),
                'section'        => 'logo',
                'settings'       => 'header_logo',
            )));

            //Footer logo
            $wp_customize->add_setting("footer_logo", array(
                'default' => '',
            ));
            $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize, 'footer_logo', array(
                'label'          => __('Footer Logo', 'devvn'),
                'section'        => 'logo',
                'settings'       => 'footer_logo',
            )));
        }

        // public function registerHeader($wp_customize)
        // {
        //     //Header
        //     $wp_customize->add_section("header", array(
        //         'title'         => __("Header", "devvn"),
        //         'priority'         => 130,
        //         'description'     => __('Description Custom footer here'),
        //     ));

        //     //Header logo
        //     $wp_customize->add_setting("header_logo", array(
        //         'default' => '',
        //     ));
        //     $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize, 'header_logo', array(
        //         'label'          => __('Header Logo', 'devvn'),
        //         'section'        => 'header',
        //         'settings'       => 'header_logo',
        //     )));
        // }

        // public function registerFooter($wp_customize)
        // {
        //     //Footer
        //     $wp_customize->add_section("footer", array(
        //         'title'         => __("Footer", "devvn"),
        //         'priority'         => 130,
        //         'description'     => __('Description Custom footer here'),
        //     ));
        //     // 14 L.E Goulburn St, Sydney 2000NSW  -  (088) 1990 6886
        //     //Footer contact
        //     // $wp_customize->add_setting("footer_contact", array(
        //     //     'default'         => '',
        //     //     'sanitize_callback' => array($this, 'santize_custom_text')
        //     // ));
        //     // $wp_customize->add_control(new WP_Customize_Control($wp_customize, "footer_contact", array(
        //     //     'label'         => __("Footer Contact", "devvn"),
        //     //     'section'         => 'footer',
        //     //     'settings'         => 'footer_contact',
        //     //     'type'             => 'text',
        //     // )));

        //     //Footer background color
        //     $wp_customize->add_setting("footer_bg_color", array(
        //         'default'         => '',
        //     ));
        //     $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bg_color', array(
        //         'label'           => 'Footer Background Color',
        //         'section'         => 'footer',
        //         'settings'       => 'footer_bg_color',

        //     )));

        //     //Footer text color
        //     // $wp_customize->add_setting("footer_text_color", array(
        //     //     'default'         => '',
        //     // ));
        //     // $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_text_color', array(
        //     //     'label'           => __('Footer Text Color', 'devvn'),
        //     //     'section'         => 'footer',
        //     //     'settings'       => 'footer_text_color',
        //     // )));

        //     //Footer logo
        //     $wp_customize->add_setting("footer_logo", array(
        //         'default' => '',
        //     ));
        //     $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize, 'footer_logo', array(
        //         'label'          => __('Footer Logo', 'devvn'),
        //         'section'        => 'footer',
        //         'settings'       => 'footer_logo',
        //     )));
        // }
    }
}
