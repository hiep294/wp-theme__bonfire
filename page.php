<?php
get_header();
?>
<div class="content-area">
    <main>
        <div class="container">
            <?php
            // If there are any posts
            if (have_posts()) :

                // Load posts loop
                while (have_posts()) : the_post();
            ?>
                    <!-- <h1 class="mt-20"><?php the_title(); ?></h1> -->
                    <div><?php the_content(); ?></div>
                <?php
                endwhile;
            else :
                ?>
                <p>Nothing to display.</p>
            <?php endif; ?>
        </div>
    </main>
    <div class="mt-50"></div>
</div>
<?php get_footer(); ?>