<?php
get_header();
// If there are any posts
if (have_posts()) :

    // Load posts loop
    while (have_posts()) : the_post();
?>
        <main>
            <section class="blogs blogs-detail">
                <div class="blogs__header">
                    <h2 class="title"><? the_title() ?></h2>
                </div>
                <? the_post_thumbnail('blog-thumbnail-portrait', array(
                    'class' => 'thumb',
                    'alt' => 'blog-thumbnail'
                )); ?>
                <div class="container content">
                    <? the_content() ?>
                </div>
                <div class="blogs-btns">
                </div>
            </section>
        </main>
    <?php
    endwhile;
else :
    ?>
    <p>Nothing to display.</p>
<?php endif;
get_footer(); ?>