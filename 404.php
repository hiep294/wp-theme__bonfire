<? get_header(); ?>

<div id="primary" class="content-area container mt-20">
    <main id="main" class="site-main">
        <div class="error-404 not-found">
            <header class="page-header">
                <h1 class="page-title">
                    <? _e("Oops! That page can&rsquo;t be found.", 'twentynineteen'); ?>
                </h1>
            </header>
            <div class="page-content">
                <p>
                    <? _e('It looks like nothing was found at this location. Maybe try a search?', 'twentynineteen'); ?>
                </p>
                <form method="get" action="<? echo get_post_type_archive_link('post'); ?>">
                    <input type="text" name="s">
                    <button><? echo _e('Submit', 'twentynineteen'); ?></button>
                </form>
            </div>
        </div>
    </main>
</div>
<div class="mt-20"></div>

<? get_footer(); ?>