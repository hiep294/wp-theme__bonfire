<div class="col-12 col-md-4">
    <div class="blog-item">
        <a href="<? the_permalink(); ?>">
            <? the_post_thumbnail('blog-thumbnail-portrait', array(
                'class' => 'thumb',
                'alt' => 'blog-thumbnail'
            )); ?>
        </a>
        <div class="blog-item__date blog-item__padding">
            <? the_time('F jS, Y') ?>
        </div>
        <div class="blog-item__title blog-item__padding">
            <h3 class="inner-text">
                <a href="<? the_permalink(); ?>">
                    <? the_title(); ?>
                </a>
            </h3>
        </div>
        <div class="blog-item__padding-line">
            <div class="line"></div>
        </div>
        <div class="blog-item__share blog-item__padding">
            <div class="share-item">
                <img src="<? echo get_theme_file_uri('/assets/images/icons/eye.png') ?>" alt="eye" />
                <span>941 Views</span>
            </div>
            <div class="share-item">
                <img src="<? echo get_theme_file_uri('/assets/images/icons/share.png') ?>" alt="share" />
                <span>27 Likes </span>
            </div>
        </div>
    </div>
</div>