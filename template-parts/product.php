<?
$price = get_post_meta(get_the_ID(), '_price', true);

global $product;
$attachment_ids = $product->get_gallery_image_ids();

$thumbnail = get_the_post_thumbnail_url(null, 'product-thumbnail');
?>

<div class="col-12 col-sm-6 col-lg-3">
    <div class="product-item">
        <div class="product-item__image-container">
            <div class="carousel-slider">
                <ul class="carousel-slider__list" id="carousel-slider__list_product-<? the_ID(); ?>">
                    <?
                    if ($thumbnail) {
                    ?>
                        <li class="carousel-slider__list__item">
                            <img src="<? echo $thumbnail; ?>" alt="product-1" class="product-thumb" />
                        </li>
                    <?
                    }
                    ?>

                    <?
                    foreach ($attachment_ids as $attachment_id) {
                        echo '<li class="carousel-slider__list__item">';
                        echo wp_get_attachment_image($attachment_id, 'product-thumbnail', false, array(
                            'class' => 'product-thumb',
                            'alt' => 'product-thumbnail'
                        ));
                        echo '</li>';
                    }
                    ?>
                </ul>
            </div>
            <div class="prev" id="prev-btn___carousel-slider__list_product-<? the_ID(); ?>"></div>
            <div class="next" id="next-btn___carousel-slider__list_product-<? the_ID(); ?>"></div>
            <div class="add-to-card-container">
                <?
                echo do_shortcode('[add_to_cart id="' . get_the_ID() . '" show_price="FALSE" style="" class="add-to-card"]');
                ?>
                <a class="other-icon" href="#">
                    <img src="<? echo get_theme_file_uri('/assets/images/icons/heart.png') ?>" alt="heart" />
                </a>
                <a class="other-icon" href="#">
                    <img src="<? echo get_theme_file_uri('/assets/images/icons/chart.png') ?>" alt="chart" />
                </a>
                <a class="other-icon" href="#">
                    <img src="<? echo get_theme_file_uri('/assets/images/icons/quick-view.png') ?>" alt="quick-view" />
                </a>
            </div>
        </div>
        <div class="product-item__text">
            <h3 class="name"><? the_title(); ?></h3>
            <div class="price"><? echo wc_price($price); ?></div>
        </div>
    </div>
</div>